#ifndef OPENRGBMONITORCONTROLPLUGIN_H
#define OPENRGBMONITORCONTROLPLUGIN_H
#include "OpenRGBPluginInterface.h"
#include "ResourceManager.h"

#include <QObject>
#include <QString>
#include <QtPlugin>
#include <QLabel>
#include "MonitorControlUi.h"

#pragma once

class ORGBPlugin : public QObject, public OpenRGBPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID OpenRGBPluginInterface_IID)
    Q_INTERFACES(OpenRGBPluginInterface)

public:
    ~ORGBPlugin() {};

    OpenRGBPluginInfo       PInfo;

    OpenRGBPluginInfo       Initialize(bool dark_theme, ResourceManager* resource_manager_ptr)   override;

    QWidget                 *CreateGUI(QWidget*)                                                 override;

    static bool             DarkTheme;
    static ResourceManager* RMPointer;
};


#endif // OPENRGBMONITORCONTROLPLUGIN_H
