QT +=                                                                                           \
    gui                                                                                         \
    core                                                                                        \
    widgets                                                                                     \

TEMPLATE = lib
DEFINES += OPENRGBMONITORCONTROLPLUGIN_LIBRARY

CONFIG += c++11

#-----------------------------------------------------------------------------------------------#
# OpenRGB Plugin SDK                                                                            #
#-----------------------------------------------------------------------------------------------#
INCLUDEPATH +=                                                                                  \
    OpenRGB/                                                                                    \
    OpenRGB/i2c_smbus/                                                                          \
    OpenRGB/net_port/                                                                           \
    OpenRGB/RGBController/                                                                      \
    OpenRGB/dependencies/json/                                                                  \

HEADERS +=                                                                                      \
    MonitorControlUi.h \
    OpenRGB/NetworkClient.h                                                                     \
    OpenRGB/NetworkProtocol.h                                                                   \
    OpenRGB/NetworkServer.h                                                                     \
    OpenRGB/OpenRGBPluginInterface.h                                                            \
    OpenRGB/ProfileManager.h                                                                    \
    OpenRGB/ResourceManager.h                                                                   \
    OpenRGB/SettingsManager.h                                                                   \
    OpenRGB/dependencies/json/json.hpp                                                          \
    OpenRGB/i2c_smbus/i2c_smbus.h                                                               \
    OpenRGB/net_port/net_port.h                                                                 \
    OpenRGB/RGBController/RGBController.h                                                       \

#-----------------------------------------------------------------------------------------------#
#  Plugin Source                                                                                #
#-----------------------------------------------------------------------------------------------#

SOURCES +=                                                                                      \
    MonitorControlUi.cpp \
    OpenRGBMonitorControlPlugin.cpp

HEADERS +=                                                                                      \
    OpenRGBMonitorControlPlugin.h                                                               \

RESOURCES += \
    resources.qrc

FORMS += \
    MonitorControlUi.ui


    win32:CONFIG(debug, debug|release) {
        win32:DESTDIR = debug
    }

    win32:CONFIG(release, debug|release) {
        win32:DESTDIR = release
    }

    win32:CONFIG += QTPLUGIN

    win32:OBJECTS_DIR = _intermediate_$$DESTDIR/.obj
    win32:MOC_DIR     = _intermediate_$$DESTDIR/.moc
    win32:RCC_DIR     = _intermediate_$$DESTDIR/.qrc
    win32:UI_DIR      = _intermediate_$$DESTDIR/.ui
