#include "OpenRGBMonitorControlPlugin.h"

bool             ORGBPlugin::DarkTheme = false;
ResourceManager* ORGBPlugin::RMPointer = nullptr;

QLabel* TabLabel()
{
    QString MonitorLabelTabString = "<html><table><tr><td width='30'><img src='";
    MonitorLabelTabString += ":/icons/monitor";
    if(ORGBPlugin::DarkTheme) MonitorLabelTabString += "_dark";
    MonitorLabelTabString += ".png' height='16' width='16'></td><td>Monitors</td></tr></table></html>";

    QLabel *MonitorTabLabel = new QLabel();
    MonitorTabLabel->setText(MonitorLabelTabString);
    MonitorTabLabel->setIndent(20);
    if(ORGBPlugin::DarkTheme)
    {
        MonitorTabLabel->setGeometry(0, 25, 200, 50);
    }
    else
    {
        MonitorTabLabel->setGeometry(0, 0, 200, 25);
    }
    return MonitorTabLabel;
}

OpenRGBPluginInfo ORGBPlugin::Initialize(bool dark_theme, ResourceManager *resource_manager_ptr)
{
    DarkTheme = dark_theme;
    RMPointer = resource_manager_ptr;

    PInfo.PluginName = "Monitor Control";
    PInfo.PluginDescription = "A Plugin for controlling monitor brightness and other things";
    PInfo.PluginLocation = "DevicesTab";

    PInfo.HasCustom = true;
    PInfo.PluginLabel = TabLabel();

    return PInfo;
}

QWidget* ORGBPlugin::CreateGUI(QWidget*)
{
    MonitorControlUi* MonCTRLUi = new MonitorControlUi({nullptr});
    return MonCTRLUi;
}
